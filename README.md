# kubernetes-kubia
simple server who can show machine hostname

### commands
```
$ docker build -t kubia .
$ docker run --name kubia-container -p 8080:8080 -d kubia
$ docker run kubia
```