import socket
from flask import Flask

app = Flask(__name__)


@app.route('/')
def hello_world():
    return "You've hit {hostname} \n".format(hostname=socket.gethostname())
